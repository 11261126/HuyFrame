package com.huyframe.ui.activity;

import android.net.VpnService;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.github.lazylibrary.util.Base64;
import com.huyframe.Presenter.MainPresenter;
import com.huyframe.R;
import com.huyframe.adapter.TestAdapter;
import com.huyframe.app.BaseActivity;
import com.huyframe.model.bean.GankDateModel;
import com.huyframe.view.test.MainView;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import me.drakeet.materialdialog.MaterialDialog;

public class MainActivity extends BaseActivity implements MainView{

    @BindView(R.id.recycleView)
    RecyclerView recyclerView;

    TestAdapter adapter;
    List<GankDateModel.ResultsBean> results;

    MaterialDialog mMaterialDialog;

    MainPresenter presenter;
    @Override
    public int getContentViewLayoutID() {
        return R.layout.activity_main;
    }

    @Override
    public void init() {
        results=new ArrayList<>();
        mMaterialDialog=new MaterialDialog(this);
        presenter=new MainPresenter(this);
        presenter.attachView(this);
        presenter.loadDatas("福利",10,1);

    }

    @Override
    public void loadData(GankDateModel data) {
            if(data!=null && !data.isError()){
                results.addAll(data.getResults());
                initAdapter();
            }
    }

    private void initAdapter() {
        if(adapter==null){
            adapter=new TestAdapter(results,this);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(adapter);
        }else{
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void showNETError(String msg,  View.OnClickListener  clickListener) {
        mMaterialDialog.setTitle("网络错误");
        mMaterialDialog.setMessage(msg);
        mMaterialDialog.setPositiveButton("重试",clickListener);
        mMaterialDialog.show();
    }

    @Override
    public void showLoading(String msg) {
        mMaterialDialog.setMessage("正在加载...");
        mMaterialDialog.show();
    }

    @Override
    public void hideLoading() {
        mMaterialDialog.dismiss();
    }

    @Override
    public void showError(String msg, View.OnClickListener clickListener) {
        mMaterialDialog.setTitle("错误");
        mMaterialDialog.setMessage(msg);
        mMaterialDialog.setPositiveButton("重试",clickListener);
        mMaterialDialog.show();
    }

    @Override
    public void showEmpty(String msg, View.OnClickListener clickListener) {
        mMaterialDialog.dismiss();
        mMaterialDialog.setTitle("无数据");
        mMaterialDialog.setMessage(msg);
        mMaterialDialog.show();
        mMaterialDialog.setPositiveButton("关闭",clickListener);
    }


}
