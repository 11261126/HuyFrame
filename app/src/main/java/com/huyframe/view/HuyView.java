package com.huyframe.view;

import android.content.DialogInterface;
import android.view.View;

/**
 * Created by BillLamian on 2016/7/13.
 */
public interface HuyView {
    /**
     * 正在加载
     * @param msg
     */
    void showLoading(String msg);

    /**
     * 加载完成
     */
    void hideLoading();

    /**
     * 错误
     * @param msg
     * @param clickListener
     */
    void showError(String msg, View.OnClickListener clickListener);

    /**
     * 数据为空
     * @param msg
     * @param clickListener
     */
    void showEmpty(String msg, View.OnClickListener clickListener);

    /**
     * 网络错误
     * @param msg
     * @param clickListener
     */
    void showNETError(String msg, View.OnClickListener clickListener);
}
