package com.huyframe.view.test;

import com.huyframe.model.bean.GankDateModel;
import com.huyframe.view.HuyView;

/**
 * Created by BillLamian on 2016/7/13.
 */
public interface MainView extends HuyView {
    void loadData(GankDateModel data);
}
