package com.huyframe.config;

import com.huyframe.utils.AndroidKit;

import java.io.File;

/**
 * Created by BillLamian on 2016/7/14.
 */
public class OkHttpConfig {
    //配置网络
    public final static long CONNECTIONTIMEOUT=6;//连接超时 单位秒


    //配置缓存目录
    public final static File CACHE_DIR = AndroidKit.getContext().getExternalCacheDir();
    //缓存大小
    public final static long CACHE_SIZE=1024*1024*10;//10M

    /**
     * 缓存内容加密或编码
     */
    public static enum ENCYPT{
        NONE,
        DES,
        BASE64
    }
}
