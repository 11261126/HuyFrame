package com.huyframe.config;

import java.io.File;

/**
 * Created by BillLamian on 2016/7/14.
 */
public class Config {
    //DEBUG模式是否开启，默认开启
    public static final boolean DEBUG_IS=true;

    //网络状态广播事件
    public static final String ACTON_NET_STATE="com.xxx.xxx.xxx";
}
