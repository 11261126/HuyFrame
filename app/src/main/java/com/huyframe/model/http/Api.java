package com.huyframe.model.http;

import com.huyframe.model.bean.GankDateModel;

import retrofit.http.GET;
import retrofit.http.Path;
import rx.Observable;

/**
 * Created by BillLamian on 2016/7/14.
 */
public interface Api {

    @GET("/api/data/{type}/{ps}/{pn}")
    Observable<GankDateModel> getData(@Path("type") String type,@Path("ps") int pageSize,@Path("pn") int pageNum);
}
