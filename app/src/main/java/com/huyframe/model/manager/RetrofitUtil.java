package com.huyframe.model.manager;

import android.content.Context;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;

/**
 * Created by BillLamian on 2016/7/14.
 */
public class RetrofitUtil {
    private static Retrofit retrofit;
    public static <T> T  create(Context context,Class<T> tClass){
        if(retrofit==null){
            synchronized (RetrofitUtil.class){
                if(retrofit==null){
                    retrofit =new Retrofit.Builder()
                            .baseUrl("http://gank.io")
                            .client(OkHttpManager.getInstance())
                            .addConverterFactory(GsonConverterFactory.create())
                            .addCallAdapterFactory(RxJavaCallAdapterFactory.create()).build();

                }
            }
        }
        return retrofit.create(tClass);

    }
}
