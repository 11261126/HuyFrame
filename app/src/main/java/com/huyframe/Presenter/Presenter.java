package com.huyframe.Presenter;

import com.huyframe.view.HuyView;

/**
 * Created by BillLamian on 2016/7/13.
 */
public interface Presenter<huyView extends HuyView> {
    /**
     * 绑定视图
     * @param view
     */
    void attachView(huyView view);

    /**
     * 释放视图
     */
    void detachView();
}
