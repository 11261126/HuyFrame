package com.huyframe.Presenter;

import com.huyframe.view.HuyView;

/**
 * Created by BillLamian on 2016/7/13.
 */
public class BasePresenter<T extends HuyView> implements Presenter<T> {

    private T tView;
    @Override
    public void attachView(T view) {
        tView=view;
    }

    @Override
    public void detachView() {
        tView=null;
    }

    public T getHuyView(){
        return tView;
    }


    public boolean isViewAttached() {
        return tView != null;
    }

    public void checkViewAttached() {
        if (!isViewAttached()) throw new IllegalArgumentException("View is Null");
    }
}
