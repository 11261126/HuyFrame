package com.huyframe.app;

import android.app.Application;
import android.content.Intent;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.github.lazylibrary.util.ToastUtils;
import com.huyframe.R;
import com.huyframe.ui.activity.MainActivity;
import com.huyframe.utils.AndroidKit;

/**
 * Created by BillLamian on 2016/7/14.
 */
public class BaseApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        AndroidKit.onApplication(getApplicationContext());
        Fresco.initialize(this);
//        Thread.setDefaultUncaughtExceptionHandler(handler);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        AndroidKit.onTerminate();
    }

//    Thread.UncaughtExceptionHandler handler=new Thread.UncaughtExceptionHandler() {
//        @Override
//        public void uncaughtException(Thread thread, Throwable ex) {
//            ToastUtils.showToast(getApplicationContext(),getResources().getString(R.string.exception_app));
//            System.exit(-1);
//            Intent intent=new Intent(getApplicationContext(), MainActivity.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            startActivity(intent);
//        }
//    };
}
