package com.huyframe.app;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.view.View;

import com.github.lazylibrary.util.ToastUtils;
import com.huyframe.R;

import butterknife.ButterKnife;
import me.drakeet.materialdialog.MaterialDialog;

/**
 * Created by BillLamian on 2016/7/13.
 */
public abstract class BaseActivity extends AppCompatActivity{

    protected MaterialDialog mMaterialDialog ;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getContentViewLayoutID()!=0){
            setContentView(getContentViewLayoutID());
            ButterKnife.bind(this);
            init();
        }else{
            throw new IllegalArgumentException(getResources().getString(R.string.exception_layout));
        }
    }



    public abstract int getContentViewLayoutID();
    public abstract void init();

}
