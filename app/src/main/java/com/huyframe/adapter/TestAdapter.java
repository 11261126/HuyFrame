package com.huyframe.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.github.lazylibrary.util.StringUtils;
import com.huyframe.R;
import com.huyframe.model.bean.GankDateModel;

import java.util.List;

/**
 * Created by BillLamian on 2016/7/18.
 */
public class TestAdapter  extends RecyclerView.Adapter<TestAdapter.HomeViewHolder>{
    List<GankDateModel.ResultsBean> list;
    Context context;
    public TestAdapter(List<GankDateModel.ResultsBean> list, Context context){
        this.list=list;
        this.context=context;
    }
    @Override
    public HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        HomeViewHolder viewHolder=new HomeViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item,parent,false));
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(HomeViewHolder holder, int position) {
        holder.title.setText(list.get(position).getDesc());
        holder.simpleDraweeView.setImageURI(Uri.parse(list.get(position).getUrl()));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class HomeViewHolder extends RecyclerView.ViewHolder{
        TextView title;
        SimpleDraweeView simpleDraweeView;
        public HomeViewHolder(View itemView) {
            super(itemView);
            title= (TextView) itemView.findViewById(R.id.title);
            simpleDraweeView= (SimpleDraweeView) itemView.findViewById(R.id.image);
        }
    }
}
