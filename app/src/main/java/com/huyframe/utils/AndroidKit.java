package com.huyframe.utils;

import android.content.Context;

/**
 * Created by BillLamian on 2016/7/14.
 */
public class AndroidKit {
    private static Context context=null;
    public static void onApplication(Context context){
        AndroidKit.context=context.getApplicationContext();
    }

    public static Context getContext(){
        return  context;
    }


    /**
     * 释放
     */
    public static void onTerminate(){
        context=null;
    }
}
